from redblob_hex_lib import *
import numpy as np
import time


def hex_neighbor_positions(position, offset=ODD):
    # convert OffsetCoord to cube offset
    offset = -1
    # todo I think this should actually be qoffset? why does it work with roffset?
    cube_pos = roffset_to_cube(offset, position)
    # get positions
    neighbor_positions = [roffset_from_cube(offset, hex_neighbor(cube_pos, 0)),
                          roffset_from_cube(offset, hex_neighbor(cube_pos, 1)),
                          roffset_from_cube(offset, hex_neighbor(cube_pos, 2)),
                          roffset_from_cube(offset, hex_neighbor(cube_pos, 3)),
                          roffset_from_cube(offset, hex_neighbor(cube_pos, 4)),
                          roffset_from_cube(offset, hex_neighbor(cube_pos, 5))]
    return neighbor_positions


def hex_neighbor_values(pos, arr):
    value_list = []
    for x in pos:
        try:
            value = arr[x[0]][x[1]]
        except:
            # if we're at an edge then we'll get some hexes which don't exist, skip those
            value = ''
        value_list.append(value)
    return value_list


#### everything below this line is super messy & just semi-random testing stuff, you should probably just ignore it ####

#
#xmax = 165
#ymax = 181
#all_nodes = []
#for x in range(xmax):
#    for y in range(ymax):
#        all_nodes.append([x, y])


# hexagonal graph
class HexGraph:
    def __init__(self):
        self.edges = {}

    def neighbors(self, id):
        return self.edges[id]


class HexGrid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.walls = []

    def in_bounds(self, id):
        (x, y) = id
        return 0 <= x < self.width and 0 <= y < self.height

    def passable(self, id):
        return id not in self.walls

    def neighbors(self, id):
        offset = -1

        (x, y) = id

        # convert to cube coordinates
        position = OffsetCoord(x, y)
        cube_pos = roffset_to_cube(offset, position)
        # get positions
        results = [roffset_from_cube(offset, hex_neighbor(cube_pos, 0)),
                   roffset_from_cube(offset, hex_neighbor(cube_pos, 1)),
                   roffset_from_cube(offset, hex_neighbor(cube_pos, 2)),
                   roffset_from_cube(offset, hex_neighbor(cube_pos, 3)),
                   roffset_from_cube(offset, hex_neighbor(cube_pos, 4)),
                   roffset_from_cube(offset, hex_neighbor(cube_pos, 5))]

        results = [(results[0][0], results[0][1]),
                   (results[1][0], results[1][1]),
                   (results[2][0], results[2][1]),
                   (results[3][0], results[3][1]),
                   (results[4][0], results[4][1]),
                   (results[5][0], results[5][1])]

        # results = [(x + 1, y), (x, y - 1), (x - 1, y), (x, y + 1)]
        if (x + y) % 2 == 0: results.reverse()  # aesthetics todo: what does this do, do I keep it?
        results = filter(self.in_bounds, results) 
        results = filter(self.passable, results)
        return results


class HexGridWeighted(HexGrid):
    def __init__(self, width, height):
        super().__init__(width, height)
        self.weights = {}

    def cost(self, from_node, to_node):
        return self.weights.get(to_node, 1)


#### TEMPORARY, COPIED FROM REDBLOB ####
import heapq


class PriorityQueue:
    def __init__(self):
        self.elements = []

    def empty(self):
        return len(self.elements) == 0

    def put(self, item, priority):
        heapq.heappush(self.elements, (priority, item))

    def get(self):
        return heapq.heappop(self.elements)[1]

def dijkstra_search(graph, start, goal):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    came_from = {}
    cost_so_far = {}
    came_from[start] = None
    cost_so_far[start] = 0

    while not frontier.empty():
        current = frontier.get()

        if current == goal:
            break

        for next in graph.neighbors(current):
            new_cost = cost_so_far[current] + graph.cost(current, next)
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost
                frontier.put(next, priority)
                came_from[next] = current

    return came_from, cost_so_far


# todo maybe merge with other things or make one class for all of this!
def set_weights(grid, weights, region, start, goal):
    start_region = region[start]
    goal_region = region[goal]

    # grid is the weighted grid, weights come from the image (color values)
    for index, weight in np.ndenumerate(weights):
        if region[index] == start_region or region[index] == goal_region:
            grid.weights[index] = weight / 1000  # todo set back to 0, this is just for testing
        elif weight == 1:
            grid.weights[index] = 0
        elif weight == 0:  # in the image 0 is 'nogo', we set to to a very high value instead
            grid.weights[index] = 99999
        elif not weight == 50:  # todo remove, this elif is just for testing!!
            grid.weights[index] = weight * 10
        else:
            grid.weights[index] = weight


# return shortest/most efficient path that was found. does this take into account cost?
def reconstruct_path(came_from, start, goal):
    current = goal
    path = []
    while current != start:
        path.append(current)
        current = came_from[current]
    path.append(start) # optional
    path.reverse() # optional
    return path


# return costs of that shortest/most efficient path - actually doesn't, it adds all of them together
# but the pathfinding func does that already. can delete this probably todo
# reworked to a test func for now
def test_get_total_cost(path, costs):
    total_cost = 0
    for idx, loc in enumerate(path):
        # total_cost = total_cost + costs[1][path[idx]]
        print(path[idx], costs[1][path[idx]])
    # return total_cost


# for testing output an image with the path highlighted as color value 255
def pathfinding_test_image(input_img, path):
    output_img = np.asarray(input_img)
    output_img.flags.writeable = True
    for (x, y) in path:
        output_img[x][y] = 255
        output_img.tofile(IMG_PATH + '_OUTPUT_TEST_' + img_imp_str, "")


# opening a RAW (bytes) image as a numpy array
def raw_img_as_np(path, x, y, flip=False):
    raw_img = open(path, 'rb').read()
    img = Image.frombytes('L', (x, y), raw_img)
    if flip:
        img = img.transpose(Image.FLIP_TOP_BOTTOM)
    img_array = np.asarray(img)  #, dtype="int32"
    return img_array


# if x and y are provided, resize to that size
def rgb_img_as_np(path, x=0, y=0, flip=False, to_hex=False):
    img = Image.open(path)

    # resize if new x & y are given
    if not x == 0 and not y == 0:
        img = img.resize((x, y), Image.NEAREST)
    if flip:
        img = img.transpose(Image.FLIP_TOP_BOTTOM)

    # convert to 3D array
    img_array = np.asarray(img, dtype=np.int32)

    # convert to 2D array if True
    if to_hex:
        img_array = tohex(img_array)
    return img_array


# returns single integer for the rgb values. use hex(array[x][y]) to get hexadecimal value
def tohex(array):
    array = np.asarray(array, dtype='uint32')
    return ((array[:, :, 0]<<16) + (array[:, :, 1]<<8) + array[:, :, 2])


def heuristic(a, b):
    (x1, y1) = a
    (x2, y2) = b
    return abs(x1 - x2) + abs(y1 - y2)


def a_star_search(graph, start, goal):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    came_from = {}
    cost_so_far = {}
    came_from[start] = None
    cost_so_far[start] = 0

    while not frontier.empty():
        current = frontier.get()

        if current == goal:
            break

        for next in graph.neighbors(current):
            new_cost = cost_so_far[current] + graph.cost(current, next)
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost + heuristic(goal, next)
                frontier.put(next, priority)
                came_from[next] = current

    return came_from, cost_so_far


#### TODO TEMPORARARY, REMOVE #####
from PIL import Image

ix, iy = 1016, 720

#img_imp_str = 'ulthuan_pathfinding.raw'
#img_imp_str = 'recreated_input_2.raw'
img_imp_str = 'recreated_input-PATHFINDING.raw'
IMG_PATH = 'C:/Users/5617812/Google Drive/tww/hex/'
#IMG_PATH = 'C:/Users/Maruka/Google Drive/tww/hex/'
image_imp_string = open(IMG_PATH + img_imp_str, 'rb').read()
img = Image.frombytes('L', (ix, iy), image_imp_string)
inp_img = np.asarray(img)


# lookup_img = raw_img_as_np(IMG_PATH + 'wh_main_lookup-ULTHUAN.raw', 194, 194)
#### ##### ##### ##### ##### #####


def pathfinding_main(x, y, start, goal, inp, lookup):
    # create new weighted grid
    hex_grid = HexGridWeighted(x, y)

    # set the weights for said grid
    set_weights(hex_grid, inp, lookup, start, goal)


    # run pathfinding algorithm
    #costs = dijkstra_search(hex_grid, start, goal)  # 14.999505043029785
    costs = a_star_search(hex_grid, start, goal) #     14.436390161514282
    #print("--- %s seconds ---" % (time.time() - start_time))

    # reconstruct the path - this is only used to display the path, which is only used for bugtesting atm.
    path = reconstruct_path(costs[0], start, goal)

    # okay i lied its also used here, but I don't think it'd need to be?
    total_costs = costs[1][path[len(path) - 1]]

    # just for testing..
    # test_get_total_cost(path, costs)

    print('done, total is..', int(round(total_costs)))
    #print("--- %s seconds ---" % (time.time() - start_time))

    # also testing
    pathfinding_test_image(inp_img, path)

#### ##### ##### ##### ##### #####
# done, total is.. 154565
# --- 19.383718490600586 seconds ---
# --- 23.01759958267212 seconds ---

# Region stuff
class Regions():
    '''Class to handle regions - loading an image, finding min/max/mid, etc.'''

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.region_names = []
        self.region_img = np.empty([width, height])
        self.region_num = 0  # possibly derive from region_names later
        self.region_color_list = []
        self.xmax, self.ymax = (0, 0)
        self.xmin, self.ymin = (0, 0)
        self.xmid, self.ymid = (0, 0)
        self.middle = (self.xmid, self.ymid)

    # add region names to the regions todo link them to a specific RGB?
    def add_region_names(self, names):
        for x in names: self.region_names.append(x)

    def load_region_img(self, path):
        # load image, flip, convert to hexadecimal
        self.region_img = rgb_img_as_np(path, self.width, self.height, True, True)
        # list of unique colors
        self.region_color_list = np.unique(self.region_img)

    def bbox(self, color):
        self.xmax, self.ymax = np.max(np.where(self.region_img == color), 1)
        self.xmin, self.ymin = np.min(np.where(self.region_img == color), 1)
        # middle of bounding box/region
        self.xmid, self.ymid = (int(round((self.xmax + self.xmin) / 2)), int(round((self.ymax + self.ymin) / 2)))
        return self.xmax, self.ymax, self.xmin, self.ymin, self.xmid, self.ymid


test_regions = Regions(1016, 720)
test_regions.load_region_img(IMG_PATH + "wh_main_lookup.bmp")

#print(np.where(test_regions.region_img == 16697174))
# print(hex(16697174))

#### ##### ##### ##### ##### #####

# print(test_regions.bbox(test_regions.region_color_list[0]))

# from region... to region ...
# ULTHUAN: 135, 18, 202
# SARTOSA: 105
bb_start = test_regions.bbox(test_regions.region_color_list[205])
bb_goal = test_regions.bbox(test_regions.region_color_list[155])  #150

# run
x0 = 1016
y0 = 720
#start0 = (13, 83)
#goal0 = (57, 12)  # 4076(4250)[4000] -- 4100
#goal0 = (81, 6)  # 5676(5850)[5450] -- 5550
start0 = (bb_start[4], bb_start[5])
goal0 = (bb_goal[4], bb_goal[5])

start_time = time.time()
pathfinding_main(x0, y0, start0, goal0, inp_img, test_regions.region_img)
print("--- %s seconds ---" % (time.time() - start_time))

