from PIL import Image
import numpy as np
from hexagon_helpers import *

path = 'C:/Users/5617812/Google Drive/tww/hex/'
file = 'ulthuan_pathfinding.raw'

img = Image.frombytes('L', (165, 181), open(path + file, 'rb').read())
hex_img = np.asarray(img)


#### TEST FUNCTIONS #####
all_nodes = []
for x in range(20):
    for y in range(10):
        all_nodes.append([x, y])

def neighbors(node):
    dirs = [[1, 0], [0, 1], [-1, 0], [0, -1]]
    result = []
    for dir in dirs:
        result.append([node[0] + dir[0], node[1] + dir[1]])
    return result

print(neighbors(all_nodes[1]))


