from redblob_hex_lib import *
from hexagon_helpers import *
import imageio
import time

import io
import numpy as np
from PIL import Image
import os


max_x = 1016
max_y = 720

# todo don't hardcode, this is fine for testing though.
map_hex_header_size = 17238

IMG_PATH = 'C:/Users/5617812/Google Drive/tww/hex/'

# opening the imgs as a numpy array
# img_imp_str = 'input.raw'  # for testing
img_imp_str = 'recreated_input_2.raw'  # actual recreated img
image_imp_string = open(IMG_PATH + img_imp_str, 'rb').read()
img = Image.frombytes('L', (max_x, max_y), image_imp_string)
inp_img = np.asarray(img)

img_river_str = 'rivers_and_transitions.raw'  # rives and transitions
image_riv_string = open(IMG_PATH + img_river_str, 'rb').read()
riv_img = Image.frombytes('L', (max_x, max_y), image_riv_string)
inp_riv_img = np.asarray(riv_img)

img_out_str = 'output.raw'

# this can probably be done much more elegantly, but it works.
out_img0 = np.asarray(img)
out_img0.flags.writeable = True
out_img1 = np.asarray(img)
out_img1.flags.writeable = True
out_img2 = np.asarray(img)
out_img2.flags.writeable = True
out_img3 = np.asarray(img)
out_img3.flags.writeable = True
out_img4 = np.asarray(img)
out_img4.flags.writeable = True
out_img5 = np.asarray(img)
out_img5.flags.writeable = True
out_img_list = [out_img0, out_img1, out_img2, out_img3, out_img4, out_img5]

# might as well clear up some memory
out_img0 = []
out_img1 = []
out_img2 = []
out_img3 = []
out_img4 = []
out_img5 = []

# color list. find in this list to get the 'index' of that color
color_list = [0, 3, 128, 131, 132, 133, 134, 135, 139, 141, 142, 146, 150]

# just for testing/debugging purposes
terrain_list = ["nogo", "transition", "river", "jungle", "sea_shallow",
                "sea_deep", "frozen", "forest", "mountain", "super_slow",
                "norsca_mountain", "grasslands", "lake"]

# todo, find out if it would be possible to NOT hardcode this damn thing
# this defines the values to be used for each 'edge'
edge_arr = np.array([[0,   17,  17,   3,   4,   5,   6,   7,  11,  13,  14,  18,  22],
                     [17,  -1, 129, 129, 130, 129, 129, 129, 129, 129, 129, 129,  23],
                     [17, 128,  -1, 138, 138, 138, 138, 138, 138, 138, 138, 138, 138],
                     [3,  128, 128, 131, 133, 143, 139, 136, 140, 144, 153, 145,  -1],
                     [4,  128, 128, 133, 132, 134,  -1,   9,  -1,  -1,  -1,  19,  -1],
                     [5,  128, 128, 143, 134, 133,  -1,  -1,  -1,  -1,  -1,  -1,  -1],
                     [6,  128, 128, 139,  -1,  -1, 134, 152, 154, 140, 145, 137,  -1],
                     [7,  128, 128, 136,   9,  -1, 152, 135, 133, 142, 139, 134,  -1],
                     [11, 128, 128, 140,  -1,  -1, 154, 133, 139, 131, 143, 135,  23],
                     [13, 128, 128, 144,  -1,  -1, 140, 142, 131, 141, 148, 143,  -1],
                     [14, 128, 128, 153,  -1,  -1, 145, 139, 143, 148, 142, 133,  -1],
                     [18, 128, 128, 145,  19,  -1, 137, 134, 135, 143, 133, 146,  21],
                     [22,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  23,  -1,  -1,  21, 150]])

# todo rivers & transition LIKELY have to be applied in a separate step
# because when they are impassible they take on the value of the underlying terrain.

# I'm referring to the hex ground types as colors here, because that's how I'm used
# to examining them. They're not actually colors, but they can take on any 256 values
# when I talk about 'assigning a color', I'm referring to the value that I assign for
# the edge between two hexes. Inspecting this output visually this doesn't look like edges,
# it looks like 6 separate images, which is why I'm used to talking about it like this.
def hex_main(img=inp_img, riv_img=inp_riv_img):
    # go through each hex, check neighbors, assign color values for edges.
    for index, x in np.ndenumerate(img):
        # a bit more clear than 'x'
        hex_color = x

        # where are we in the table?
        position = OffsetCoord(index[0], index[1])

        # position of neighbors
        neighbor_positions = hex_neighbor_positions(position, ODD)
        # values of neighbors
        hex_values = hex_neighbor_values(neighbor_positions, img)

        # are we on a river or transition hex?
        riv_hex = False
        riv_hex_color = riv_img[position[0]][position[1]]

        if riv_hex_color == 3 or riv_hex_color == 128:
            riv_hex = True
            # print('True: ', position, 'is a', terrain_list[color_list.index(riv_hex_color)])

        # river neighbor values (to check whether there's a river or transition neighbor)
        riv_hex_values = hex_neighbor_values(neighbor_positions, riv_img)

        # looping through all directions, checking colors, then setting the right color
        i = 0
        while i < 6:  # i is the direction
            # set origin hex
            if riv_hex:
                hex_color_origin = riv_hex_color
            else:
                hex_color_origin = hex_color

            # set comparison hex. if this isn't 255 then this neighbour is a river or transition!
            riv_neigh = False
            if not riv_hex_values[i] == 255:
                riv_neigh = True
                comparison_hex = riv_hex_values[i]
            else:
                comparison_hex = hex_values[i]

            # if both the origin & comparison hex are river/transition, then...
            if riv_hex and riv_neigh:
                hex_color_origin = 0
                comparison_hex = hex_color

            #if riv_hex or riv_neigh:
            #    print('Riv:', riv_hex, 'neigh', riv_neigh, 'pos', position)
            # todo this can probably be removed, it was just for testing.
            try:
                edge_color = find_edge_color(hex_color_origin, comparison_hex, position, i)
            except:
                print('ERROR: pos:', position, 'riv:', riv_hex, 'riv_n', riv_neigh,
                      'hex_color', hex_color, 'hex_comp', hex_values[i], 'riv_color',
                      riv_hex_color, 'riv_comp', riv_hex_values[i])

            # todo do this without an exception ..
            # if neighbor_positions[i][1] < max_x & neighbor_positions[i][0] < max_y:
            try:
               # assign the edge color
               out_img_list[i][neighbor_positions[i][0]][neighbor_positions[i][1]] = edge_color
            except:
                # todo this is useless, literally
                a = 0
            i += 1

    # saving image. works well, could probably be done more neatly.
    out_img = interweave_arrays(out_img_list[0], out_img_list[1], out_img_list[2],
                                out_img_list[3], out_img_list[4], out_img_list[5])
    out_img.tofile(IMG_PATH + img_out_str, "")
    print('saved', IMG_PATH + img_out_str)


# testing version, with some hardcoded values.
def hex_main_test(img=inp_img):
    loop = True

    if loop:
        for index, x in np.ndenumerate(img):
            hex_color = x
            # print(index)
            if x == 32:
                # where are we in the table?
                position = OffsetCoord(index[0], index[1])
                # position of neighbors
                neighbor_positions = hex_neighbor_positions(position, ODD)
                # values of neighbors
                values = hex_neighbor_values(neighbor_positions, img)

                # looping through all directions, checking colors, then setting the right color
                i = 0
                while i < 6:
                    direction = i
                    # print('dir', direction)
                    if values[direction] == 16:
                        # testing: set pixel to 255 if conditions are met
                        out_img_list[i][neighbor_positions[direction][0]][neighbor_positions[direction][1]] = 255
                    i += 1
    # saving image. works well, could probably be done more neatly.
    out_img = interweave_arrays(out_img_list[0], out_img_list[1], out_img_list[2],
                                out_img_list[3], out_img_list[4], out_img_list[5])
    out_img.tofile(IMG_PATH + img_out_str, "")
    print('saved', IMG_PATH + img_out_str)


def interweave_arrays(arr0, arr1, arr2, arr3, arr4, arr5):
    # flatten arrays
    arr0 = arr0.flatten()
    arr1 = arr1.flatten()
    arr2 = arr2.flatten()
    arr3 = arr3.flatten()
    arr4 = arr4.flatten()
    arr5 = arr5.flatten()

    # interleave arrays
    arr6 = np.empty((arr0.size + arr1.size + arr2.size + arr3.size + arr4.size + arr5.size,), dtype=arr0.dtype)
    # order of directions is different in pathfinding and redblob's code, so change that here.
    arr6[0::6] = arr3
    arr6[1::6] = arr2
    arr6[2::6] = arr1
    arr6[3::6] = arr0
    arr6[4::6] = arr5
    arr6[5::6] = arr4

    return arr6


def find_edge_color(hex_color, neigh_color, position, i):
    # takes the color values of the 'origin' hex (the one that you're doing the
    # comparison from, and the one from the neighbouring hex. Returns the color that the
    # hex in the right channel will need to be.
    # all the color values probably have to be hardcoded.

    # if they're the same then the edge will also be the same, in most cases
    if hex_color == neigh_color:
        edge_color = hex_color
    # todo I need to figure out how to handle the literal edge cases (edge of the map) properly & in 1 place
    elif neigh_color == '':
        edge_color = ''
    # if the neighbor is 0 (nogo) then the edge will also be nogo.
    # todo maybe explain, or you'll just have to trust me eh?
    elif neigh_color == 0:
        edge_color = 0

    # first draft of river handling. If river + river then you get a nogo edge, but the edge
    # depends on the underlying terrain.. so either we need to do this with a separate input image
    # or we need to guesstimate todo I think this will have to be done in a separate pass...
    elif hex_color == 18 and neigh_color == 18:
        edge_color = 18  # todo this is just set to 18 for now, but that's only for debugging...
    else:
        # print(hex_color, neigh_color)
        edge_color = edge_arr[color_list.index(hex_color)][color_list.index(neigh_color)]

        # for testing purposes: todo remove
        if edge_color == -1:
            edge_color = 255

            debug = True
            if not hex_color == 3 and not neigh_color == 3 and debug:
                print("ERROR: " + str(position), str(i) + "  No edge defined between " +
                      terrain_list[color_list.index(hex_color)] + " (" + str(hex_color) + ") and " +
                      terrain_list[color_list.index(neigh_color)] + " (" + str(neigh_color) + ")")
    return edge_color


# def rivers_and_transitions():


def test__pathfinding_values():
    img_imp_str = "pathfinding_6c.raw"
    image_string = open(IMG_PATH + img_imp_str, 'rb').read()
    img = Image.frombytes('L', (max_x, max_y * 6), image_string)
    inp_img = np.asarray(img)

    print(inp_img.shape)
    uniques = np.unique(inp_img)

    print(uniques)


