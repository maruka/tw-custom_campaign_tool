def u8(f):
    u8 = int.from_bytes(f.read(1), byteorder='little')
    return u8


def u16(f):
    u16 = int.from_bytes(f.read(2), byteorder='little')
    return u16


def u32(f):
    u32 = int.from_bytes(f.read(4), byteorder='little')
    return u32


def i8(f):
    i8 = int.from_bytes(f.read(1), byteorder='little', signed=True)
    return i8


def i16(f):
    i16 = int.from_bytes(f.read(2), byteorder='little', signed=True)
    return i16


def i32(f):
    i32 = int.from_bytes(f.read(4), byteorder='little', signed=True)
    return i32


def String(f, length=0):
    # Length = length of prefix which is an int for length of string.
    String = f.read(int.from_bytes(f.read(length), byteorder='little')).decode('utf-8')
    return String


# skip X amount of bytes
def skip_bytes(f, n_bytes):
    f.seek(f.tell() + n_bytes)