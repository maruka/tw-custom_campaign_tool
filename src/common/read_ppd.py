from decode_helpers import *

path = 'C:/Users/5617812/Google Drive/tww/hex/'
file = 'pathfinding.ppd'

region_list = []
movement_cost_list = []
subregion_continent_list = []

def decode_ppd():
    with open(path + file, 'rb') as f:
        # header 6 bytes
        f.read(8)

        # file version
        pathfinding_vesion = u32(f)

        #### REGION DATA ####
        # number of regions
        region_numbers = u32(f)

        for region in range(region_numbers):
            region_list.append(String(f, 4))

        #### PATHFINDING 'IMAGES" ####
        # map / image x & y
        map_x_y = [u16(f), u16(f)]

        # skip the 'images' for now, there's 8 of them
        skip_bytes(f, map_x_y[0] * map_x_y[1] * 8)

        #### MOVEMENT COSTS ####
        movement_cost_num = u32(f)

        for i in range(movement_cost_num):
            movement_cost_list.append(u16(f))

        #### SUBREGION 'CONTINTENT' aka FRED ####

        print(f.tell())

        fred_num = u16(f)
        for i in range(fred_num):
            subregion_continent_list.append(u16(f))

        print(subregion_continent_list)
        print(len(region_list))

        print(f.tell())




