from builder import *
from graphics import *
import redblob_hex_lib as rb
import numpy as np
from PIL import Image
# import cairo
import tkinter

# hex size:
hex_size = 2
# origin
origin = rb.Point(hex_size, hex_size)
# default layout:
hex_layout = rb.Layout(rb.layout_flat, hex_size, origin)

# window size todo don't hardcode?
win_max_x = 500
win_max_y = 500

root_path = 'C:/Users/5617812/Google Drive/tww/hex/'
out_base = 'svg_out'
out_postfix = '_borders'
svg_file = out_base + out_postfix + '.svg'

# hex_inp_img = inp_img

img_x = 1016
img_y = 720

# hex_inp_str = 'test_hexmap.raw'
# hex_inp_str = 'recreated_input_2.raw'
# hex_inp_str = 'roads.raw'
hex_inp_str = 'borders.raw'
# todo this is just for testing
hex_imp_string = open(IMG_PATH + hex_inp_str, 'rb').read()
hex_img = Image.frombytes('L', (img_x, img_y), hex_imp_string)
hex_inp_img = np.asarray(hex_img)


def graphics_main_test():
    win = GraphWin("My Circle", 100, 100)
    c = Circle(Point(50,50), 10)
    c.draw(win)
    p = Polygon(Point(1, 1), Point(5, 3), Point(2, 7))
    p.draw(win)
    win.getMouse() # Pause to view result
    win.close()    # Close window when done


# todo what do these x and y's do again..?
def graphics_main(input = hex_inp_img, x0 = 0, y0 = 0, x1 = 0, y1 = 0):
    point_list = []

    image_shape = input.shape
    # handling incorrect / out of bounds restraints
    if 0 > x0 > image_shape[0] or x0 >= x1:
        x0 = 0
    if 0 > y0 > image_shape[1] or y0 >= y1:
        y0 = 0
    if x1 == 0 or 0 > x1 > image_shape[0]:
        x1 = image_shape[0]
    if y1 == 0 or 0 > y1 > image_shape[1]:
        y1 = image_shape[1]

    input_hex_map = input[x0:x1, y0:y1]

    for index, hex_color in np.ndenumerate(input_hex_map):
        position = index[0], index[1]
        point_list.append([position[1], position[0]])

    draw_list = corner_helper(point_list)
    color_values = input_hex_map.flatten()

    # draw_to_screen(draw_list, color_values)
    # draw_to_svg(draw_list, color_values, input_hex_map)
    draw_tkinter(draw_list, color_values)


def draw_to_screen(draw_list, color_values):
    win = GraphWin("Map display", win_max_x, win_max_y)
    i2 = 0  # for the color value
    for i in draw_list:
        h = Polygon(i)
        color = color_values[i2]
        h.setFill(color_rgb(color, color, color))
        h.draw(win)
        i2 += 1
    win.getMouse()
    win.close()


def draw_tkinter(draw_list, color_values):
    start_time = time.time()
    print('Start drawing!')

    # unique_color_list = list(np.unique(inp_image))

    top = tkinter.Tk()

    C = tkinter.Canvas(top, bg="blue", height=600, width=600)

    for idx, hex in enumerate(draw_list):
        hex_fill = '#%02x%02x%02x' % (color_values[idx], color_values[idx], color_values[idx])

        C.create_polygon(hex, fill=hex_fill, outline='black')

    print("--- %s seconds ---" % (time.time() - start_time))
    print('start pack')
    C.pack()
    top.mainloop()
    print('did the thing')


def draw_to_svg(draw_list, color_values, inp_image):
    # unique colors:
    unique_color_list = list(np.unique(inp_image))

    viewbox_x = inp_image.shape[0] * hex_size * 1.5
    viewbox_y = inp_image.shape[1] * hex_size * 1.5

    with open(root_path + svg_file, "w") as f:
        # header
        f.write('<?xml version="1.0" encoding="utf-8"?>\n'
                '<!-- Generator: Maruka\'s MapToSVG tool . SVG Version: 6.00 Build 0)  -->\n'
                '<svg xmlns="http://www.w3.org/2000/svg"'
                ' xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n'
                '	width="' + str(viewbox_x) +'"\n'
                '	height="' + str(viewbox_y) +'"\n'
                '	viewBox="0 0 ' + str(viewbox_x) + ' ' + str(viewbox_y) +
                '" style="enable-background:new 0 0 1366 768;" xml:space="preserve">\n')

        # styles list
        f.write('<style type="text/css">\n')
        for color in unique_color_list:
            color_hex = '#%02x%02x%02x' % (color, color, color)
            stroke_width = str(hex_size / 10)
            f.write('	.st' + str(color) + '{fill:' + color_hex +
            #f.write('	.st' + str(color) + '{fill:' + color_hex + '; stroke: ' + color_hex + '; stroke-width: ' + stroke_width +
                    ';}\n')
        f.write('</style>\n')

        # print(unique_color_list)
        # writing each hex
        for idx, hex in enumerate(draw_list):
            # which class (color in this case) should the hex get?
            svg_class = color_values[idx]

            if not svg_class == 0:  # set to 0 to not draw black hexes
                f.write('	<polygon class="st' + str(svg_class) + '" points="')
                for x, y in zip(hex[::2],hex[1::2]):
                    f.write(str(x) + ',' + str(y) + ' ')
                f.write('	"/>\n')

        # footer
        f.write('</svg>')


def corner_helper(point_list):
    corner_point_list = []

    # print(point_list)

    hex_corner_points_list = []

    i = 0
    while i < len(point_list):
        position = OffsetCoord(point_list[i][0], point_list[i][1])

        cube_coords = rb.qoffset_to_cube(-1, (rb.OffsetCoord(point_list[i][0], point_list[i][1])))

        hex_corners = rb.polygon_corners(hex_layout, cube_coords)

        temp_list = []
        for x, y in hex_corners:
            temp_list.extend([Point(x, y)])
            #temp_list.extend([x, y])

        hex_corner_points_list.append(temp_list)

        i += 1

    return hex_corner_points_list


def test_tinker():
    dots = [[1, 1], [2, 1], [2, 2], [1, 2], [0.5, 1.5]]
    c = Canvas(width=750, height=750)
    c.pack()
    out = []
    for x, y in dots:
        out += [x * 250, y * 250]
    c.create_polygon(*out, fill='#aaffff')  # fill with any color html or name you want, like fill='blue'
    # c.update(

